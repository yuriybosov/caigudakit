//
//  CTDataBaseRequest.h
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTRequest.h"
#import "CTStorage.h"

@interface CTDataBaseRequest : CTRequest
{
    CTStorage* storage;

    BOOL cancelled;
    BOOL executing;
}

@property (nonatomic, strong) NSFetchRequest* fetchRequest;

- (id)initWithStorage:(CTStorage*)aStorage;

/**
 * Execute request synchronously
 **/
- (void)execute;

@end