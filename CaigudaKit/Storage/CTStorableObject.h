//
//  CTStorableObject.h
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol CTStorableObject <NSObject>

+ (NSManagedObjectID*)isStorageContainedObject:(NSManagedObject*)anObject;

- (void)setupWithDictionary:(NSDictionary*)aData;

/**
 * Insert into context without immediate save into storage
 **/
- (void)insertIntoContext;

@optional

+ (NSArray*)defaultSortDescriptors;

/**
 * Determine not updatable attributes in model during update (see insertModelsWithUpdate:modelAttributesToUpdate: in CTStorable)
 **/
+ (NSSet*)notUpdatableAttributes;

@end
