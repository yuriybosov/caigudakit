//
//  MUGatewayRequest.m
//  CaigudaKit
//
//  Created by Malaar on 12.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGatewayRequest.h"
#import "CTGateway.h"
#import "CTQueueNode.h"

@interface CTRequestUploadNode : CTQueueNode

@property (nonatomic, copy) CTGatewayRequestUploadProgressBlock block;

@end

@interface CTRequestDownloadNode : CTQueueNode

@property (nonatomic, copy) CTGatewayRequestDownloadProgressBlock block;

@end



@implementation CTGatewayRequest

@synthesize path, type, parameters;
@synthesize successBlock, failureBlock;

#pragma mark - Init/Dealloc

- (id)init
{
    NSAssert(NO, @"Don't use this method!");
    return nil;
}

- (id)initWithGateway:(CTGateway*)aGateway
{
    self = [super init];
    if (self)
    {
        gateway = aGateway;
        uploadProgressBlocks = [NSMutableArray new];
        downloadProgressBlocks = [NSMutableArray new];
        headers = [NSMutableDictionary new];
    }
    return self;
}

- (id)initWithGateway:(CTGateway *)aGateway preparedURLRequest:(NSURLRequest *)anURLRequest
{
    self = [self initWithGateway:aGateway];
    if (self)
    {
        preparedURLRequest = anURLRequest;
    }
    return self;
}

- (void)dealloc
{
    self.successFailureDispatchQueue = NULL;
}

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
- (void)setSuccessFailureDispatchQueue:(dispatch_queue_t)successFailureDispatchQueue
{
    if (_successFailureDispatchQueue != successFailureDispatchQueue)
    {
        if (_successFailureDispatchQueue)
        {
            dispatch_release(_successFailureDispatchQueue);
            _successFailureDispatchQueue = NULL;
        }
        
        if (successFailureDispatchQueue)
        {
            dispatch_retain(successFailureDispatchQueue);
            _successFailureDispatchQueue = successFailureDispatchQueue;
        }
    }
}
#endif

#pragma mark - Internet reachability

- (BOOL)isInternetReachable
{
    return [gateway isInternetReachable];
}

#pragma mark - Request execute

- (BOOL)canExecute
{
    return [self isInternetReachable];
}

- (void)start
{
    operation = [gateway startRequest:self];
    CTLog(@"\nCTGatewayRequest start operation:\n%@ \nWith params: %@ \nWith headers: %@", operation, parameters, headers);
}

- (BOOL)isExecuting
{
    return [operation isExecuting];
}

- (void)cancel
{
    [operation cancel];
}

- (BOOL)isCancelled
{
    return [operation isCancelled];
}

- (BOOL)isFinished
{
    return [operation isFinished];
}

#pragma mark - Configure request callbacks

- (void)setupSuccessBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
             failureBlock:(CTGatewayRequestFailureBlock)aFailureBlock
            dispatchQueue:(dispatch_queue_t)aDispatchQueue
{
    NSParameterAssert(aDispatchQueue);

    successBlock = [aSuccessBlock copy];
    failureBlock = [aFailureBlock copy];
    self.successFailureDispatchQueue = aDispatchQueue;
}

- (void)addUploadProgressBlock:(CTGatewayRequestUploadProgressBlock)anUploadProgressBlock
                 dispatchQueue:(dispatch_queue_t)aDispatchQueue
{
    NSParameterAssert(anUploadProgressBlock);
    NSParameterAssert(aDispatchQueue);
    
    CTRequestUploadNode* node = [CTRequestUploadNode new];
    node.block = anUploadProgressBlock;
    node.dispatchQueue = aDispatchQueue;
    [uploadProgressBlocks addObject:node];
}

- (void)addDownloadProgressBlock:(CTGatewayRequestDownloadProgressBlock)aDownloadProgressBlock
                   dispatchQueue:(dispatch_queue_t)aDispatchQueue
{
    NSParameterAssert(aDownloadProgressBlock);
    NSParameterAssert(aDispatchQueue);
    
    CTRequestDownloadNode* node = [CTRequestDownloadNode new];
    node.block = aDownloadProgressBlock;
    node.dispatchQueue = aDispatchQueue;
    [downloadProgressBlocks addObject:node];
}

#pragma mark - Execute Request callbacks

- (void)executeSuccessBlockWithOperation:(AFHTTPRequestOperation*)anOperation responseObject:(id)aResponseObject
{
    if (successBlock)
    {
        if (self.successFailureDispatchQueue)
        {
            dispatch_async(self.successFailureDispatchQueue, ^
            {
                CTResponse* response = successBlock(anOperation, aResponseObject);
                if(self.executeAllResponseBlocksSync)
                    [self executeSynchronouslyAllResponseBlocksWithResponse:response];
                else
                    [self executeAllResponseBlocksWithResponse:response];
            });
        }
    }
}

- (void)executeFailureBlockWithOperation:(AFHTTPRequestOperation*)anOperation error:(NSError*)anError
{
    if (failureBlock)
    {
        if (self.successFailureDispatchQueue)
        {
            dispatch_async(self.successFailureDispatchQueue, ^
            {
                CTResponse* response = failureBlock(anOperation, anError);
                if(self.executeAllResponseBlocksSync)
                    [self executeSynchronouslyAllResponseBlocksWithResponse:response];
                else
                    [self executeAllResponseBlocksWithResponse:response];
            });
        }
    }
}

- (void)executeAllUploadProgressBlocksWithBytesWriten:(NSUInteger)aBytesWritten
                                     totalBytesWriten:(long long)aTotalBytesWritten
                            totalBytesExpectedToWrite:(long long)aTotalBytesExpectedToWrite
{
    for(CTRequestUploadNode* node in uploadProgressBlocks)
    {
        if (node.dispatchQueue && node.block)
        {
            dispatch_async(node.dispatchQueue, ^
            {
                node.block(self, aBytesWritten, aTotalBytesWritten, aTotalBytesExpectedToWrite);
            });
        }
    }
}

- (void)executeAllDownloadProgressBlocksWithBytesRead:(NSUInteger)aBytesRead
                                       totalBytesRead:(long long)aTotalBytesRead
                             totalBytesExpectedToRead:(long long)aTotalBytesExpectedToRead
{
    for(CTRequestDownloadNode* node in downloadProgressBlocks)
    {
        if (node.dispatchQueue && node.block)
        {
            dispatch_async(node.dispatchQueue, ^
            {
                node.block(self, aBytesRead, aTotalBytesRead, aTotalBytesExpectedToRead);
            });
        }
    }
}

#pragma mark - Headers

- (void)addValue:(NSString*)aHeaderValue forHeaderField:(NSString*)aHeaderField
{
    [headers setObject:aHeaderValue forKey:aHeaderField];
}

- (void)clearHeaders
{
    [headers removeAllObjects];
}

#pragma mark - Prepare request

- (NSMutableURLRequest*)urlRequest
{
    NSMutableURLRequest* urlRequest = (preparedURLRequest) ? [preparedURLRequest mutableCopy] : [gateway.httpClient requestWithMethod:self.type path:self.path parameters:self.parameters];
    
    [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
    {
        [urlRequest addValue:obj forHTTPHeaderField:key];
    }];
    
    return urlRequest;
}

#pragma mark - I/O Stream

- (NSInputStream *)inputStream
{
    if (operation)
        return operation.inputStream;
    
    return _inputStream;
}

- (NSOutputStream *)outputStream
{
    if (operation)
        return operation.outputStream;
    
    return _outputStream;
}

@end



@implementation CTRequestUploadNode

@end

@implementation CTRequestDownloadNode

@end
