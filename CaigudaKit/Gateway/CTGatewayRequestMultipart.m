//
//  CTGatewayRequestMultipart.m
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGatewayRequestMultipart.h"
#import "CTGateway.h"

@implementation CTGatewayRequestMultipart

#pragma mark - Init

- (id)initWithGateway:(CTGateway*)aGateway
{
    self = [super initWithGateway:aGateway];
    if(self)
    {
        constructingBlocks = [NSMutableArray array];
    }
    return self;
}

- (id)initWithGateway:(CTGateway *)aGateway preparedURLRequest:(NSURLRequest *)anURLRequest
{
    NSAssert(NO, @"This initialization method cannot be used in multipart request");
    return nil;
}

#pragma mark - Multipart

- (void)addConstructingMultipartFormDataBlock:(CTConstructingMultipartFormDataBlock)block
{
    [constructingBlocks addObject:[block copy]];
}

#pragma mark - Prepare request

- (NSMutableURLRequest*)urlRequest
{
    NSMutableURLRequest* urlRequest = [gateway.httpClient multipartFormRequestWithMethod:self.type
                                                                                    path:self.path
                                                                              parameters:self.parameters
                                                               constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
    {
        for (CTConstructingMultipartFormDataBlock block in constructingBlocks)
        {
            block((id<CTMultipartFormData>)formData);
        }
    }];
    
    [headers enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
     {
         [urlRequest addValue:obj forHTTPHeaderField:key];
     }];

    return urlRequest;
}

@end
