//
//  MUTargetAction.h
//  CaigudaKitMaster
//
//  Created by Malaar on 3/14/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTTargetAction : NSObject

@property (nonatomic, weak, readonly) id target;
@property (nonatomic, readonly) SEL action;

+ (CTTargetAction*)targetActionWithTarget:(id)aTarget action:(SEL)anAction;

- (id)initWithTarget:(id)aTarget action:(SEL)anAction;

- (void)setTarget:(id)aTarget action:(SEL)anAction;
- (void)sendActionFrom:(id)aSender;

@end


@interface CTTargetActionList : NSObject
{
    NSMutableArray* taList;
}

- (void)addTarget:(id)aTarget action:(SEL)anAction;
- (void)sendActionsFrom:(id)aSender;

@end


@protocol CTTargetActionListProtocol <NSObject>

@property (nonatomic, readonly) CTTargetActionList* taList;

- (void)addTarget:(id)aTarget action:(SEL)anAction;

@end