//
//  CTPair.m
//  CaigudaKit
//
//  Created by Malaar on 10/13/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTPair.h"

@implementation CTPair

+ (CTPair*)pairWithFirst:(id)aFirst second:(id)aSecond
{
    return [[[self class] alloc] initWithFirst:aFirst second:aSecond];
}

- (id)initWithFirst:(id)aFirst second:(id)aSecond
{
    if((self = [super init]))
    {
        self.first = aFirst;
        self.second = aSecond;
    }
    
    return self;
}

@end


@implementation NSArray (CTPair)

- (CTPair*)pairByFirst:(id)aFirst
{
    CTPair* result = nil;
    for(CTPair* pair in self)
    {
        if([pair.first isEqual:aFirst])
        {
            result = pair;
            break;
        }
    }
    return result;
}

- (CTPair*)pairBySecond:(id)aSecond
{
    CTPair* result = nil;
    for(CTPair* pair in self)
    {
        if([pair.second isEqual:aSecond])
        {
            result = pair;
            break;
        }
    }
    return result;
}

@end