//
//  MUTargetAction.m
//  CaigudaKitMaster
//
//  Created by Malaar on 3/14/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTTargetAction.h"

@implementation CTTargetAction

@synthesize target;
@synthesize action;

+ (CTTargetAction*)targetActionWithTarget:(id)aTarget action:(SEL)anAction
{
    return [[[self class] alloc] initWithTarget:aTarget action:anAction];
}

- (id)initWithTarget:(id)aTarget action:(SEL)anAction
{
    if ((self = [super init]))
    {
        [self setTarget:aTarget action:anAction];
    }
    return self;
}

- (void)setTarget:(id)aTarget action:(SEL)anAction
{
    target = aTarget;
    action = anAction;
}

- (void)sendActionFrom:(id)aSender
{
    if(action && [target respondsToSelector:action])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"        
        [target performSelector:action withObject:aSender];
#pragma clang diagnostic pop
    }
}

@end


@implementation CTTargetActionList

- (id)init
{
    if( (self = [super init]) )
    {
        taList = [NSMutableArray new];
    }
    return self;
}

- (void)addTarget:(id)aTarget action:(SEL)anAction
{
    [taList addObject:[[CTTargetAction alloc] initWithTarget:aTarget action:anAction]];
}

- (void)sendActionsFrom:(id)aSender
{
    for(CTTargetAction* ta in taList)
    {
        [ta sendActionFrom:aSender];
    }
}

@end
