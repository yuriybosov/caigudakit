//
//  NSString+URLCoding.h
//  CaigudaKit
//
//  Created by Malaar on 11.07.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLCoding)

- (NSString*)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
- (NSString*)urlDecodeUsingEncoding:(NSStringEncoding)encoding;

@end
