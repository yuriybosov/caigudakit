//
//  NSDictionary+NullProtected.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NullProtected)

- (id)nullProtectedObjectForKey:(id)aKey;
- (id)nullProtectedObjectForKeyPath:(id)aKeyPath;

- (NSDictionary*)dictionaryCleanedFromNulls;

@end


@interface NSMutableDictionary (NilProtected)

- (void)setNilProtectedObject:(id)anObject forKey:(id<NSCopying>)aKey;

@end