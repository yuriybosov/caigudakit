//
//  UIImage+fixOrientation.h
//  Vivaturizmo-iOS6
//
//  Created by Malaar on 17.10.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
