//
//  MUStackedView.h
//  CaigudaKit
//
//  Created by Malaar on 10/6/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CTStackedView;

@protocol CTStackedViewDelegate <NSObject>

@optional
- (void)stackedView:(CTStackedView*)aStackedView willChangeFromIndex:(NSUInteger)aFromIndex toIndex:(NSUInteger)aToIndex;
- (void)stackedView:(CTStackedView*)aStackedView didChangedFromIndex:(NSUInteger)aFromIndex toIndex:(NSUInteger)aToIndex;

@end


@interface CTStackedView : UIView
{
    NSMutableArray* stackedSubviews;
    UIView* currentView;
}

@property (nonatomic, weak) id<CTStackedViewDelegate> delegate;
@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, assign) UIView* currentStackedSubview;
@property (nonatomic, readonly) NSUInteger countStackedSubviews;

- (void)addStackedSubview:(UIView *)aView;
- (void)insertStackedSubview:(UIView *)aView atIndex:(NSUInteger)aIndex;
- (void)removeStackedSubviewAtIndex:(NSUInteger)aIndex;
- (void)removeStackedSubview:(UIView *)aView;
- (void)removeAllStackedSubviews;

@end
