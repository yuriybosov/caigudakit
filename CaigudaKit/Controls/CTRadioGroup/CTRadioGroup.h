//
//  CTRadioGroup.h
//
//
//  Created by Alexander on 6/10/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

@class CTRadioGroup;

@protocol CTRadioItem <NSObject>

@required
@property (nonatomic, weak) CTRadioGroup *radioGroup;
@property (nonatomic, readonly) BOOL radioItemSelected;

- (void)setRadioItemSelected:(BOOL)selected animated:(BOOL)animated;

@optional
@property (nonatomic, getter=isEnabled) BOOL enabled;

@end


@protocol CTRadioGroupDelegate <NSObject>

@optional
- (BOOL)radioGroup:(CTRadioGroup *)radioGroup shouldSelectItem:(id<CTRadioItem>)item atIndex:(NSUInteger)index;
/**
 * If isMultiSelectionEnabled YES fromIndex will return NSNotFound
 **/
- (void)radioGroup:(CTRadioGroup *)radioGroup selectedItemIndexChangedTo:(NSUInteger)toIndex from:(NSUInteger)fromIndex;

@end

@interface CTRadioGroup : NSObject
{
    NSUInteger selectedIndex; // can be NSNotFound
    
    NSMutableArray *radioItems;
}

@property (nonatomic, weak) id<CTRadioGroupDelegate> delegate;

@property (nonatomic, assign) BOOL isMultiSelectionEnabled; /// by default is NO
@property (nonatomic, assign) BOOL isEnabled;               /// by default is YES

@property (nonatomic, readonly) NSArray *items;
@property (nonatomic, readonly) NSUInteger itemsCount;
@property (nonatomic, readonly) NSArray *selectedItemIndexes; /// array of index NSNumbers

- (id)initWithItems:(NSArray *)items;

- (void)setupItems:(NSArray *)items;
- (void)addItem:(id<CTRadioItem>)item;
- (void)removeItemAtIndex:(NSUInteger)index;

- (void)selectItemAtIndex:(NSUInteger)index animated:(BOOL)animated; /// programmaticaly switch to item at index
- (void)selectItem:(id<CTRadioItem>)item animated:(BOOL)animated;

- (id<CTRadioItem>)selectedItem;                /// will return nil if isMultiSelectionEnabled is YES
- (NSUInteger)selectedItemIndex;                /// will return NSNotFound if isMultiSelectionEnabled is YES

- (void)unselectAllItemsAnimated:(BOOL)animated; // selectedItemIndexes will return empty array

@end
