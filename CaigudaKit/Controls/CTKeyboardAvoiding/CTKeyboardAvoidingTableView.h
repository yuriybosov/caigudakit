//
//  MUKeyboardAvoidingTableView.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 1/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//
//  Taken as a basis for an example of "TPKeyboardAvoidingTableView"(Created by Michael Tyson)

#import <UIKit/UIKit.h>
#import "CTKeyboardAvoidingProtocol.h"
#import "CTKeyboardToolbar.h"

@interface CTKeyboardAvoidingTableView : UITableView <CTKeyboardAvoidingProtocol, CTKeyboardToolbarProtocol>
{
    UIEdgeInsets    _priorInset;
    BOOL            _keyboardVisible;
    CGRect          _keyboardRect;
    NSMutableArray *_objectsInKeyboard;
    NSMutableDictionary* _indexPathseObjectsInKeyboard;
    
    NSUInteger _selectIndexInputField;
    id _inputField;
    CTKeyboardToolbar *keyboardToolbar;
    
    BOOL            _isAnimated;
}

- (void)sordetInputTraits:(NSArray*)inputTraits byIndexPath:(NSIndexPath*)indexPath;

@end
