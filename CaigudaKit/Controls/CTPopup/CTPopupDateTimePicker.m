//
//  MUPopupDateTimePicker.m
//  CaigudaKit
//
//  Created by Malaar on 11/21/11.
//  Copyright (c) 2011 Caiguda. All rights reserved.
//

#import "CTPopupDateTimePicker.h"

@implementation CTPopupDateTimePicker

#pragma mark - override next methods to customize:

- (UIView*)createPicker
{
    UIDatePicker* pv = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
    pv.datePickerMode = UIDatePickerModeDateAndTime;
    
    return pv;
}

@end
