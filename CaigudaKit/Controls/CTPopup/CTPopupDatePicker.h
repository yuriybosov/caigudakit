//
//  MUPopupDatePicker.h
//  CaigudaKit
//
//  Created by Malaar on 9/23/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPopupPicker.h"

@interface CTPopupDatePicker : CTPopupPicker

@property (nonatomic, readonly) UIDatePicker* popupedPicker;

@end
