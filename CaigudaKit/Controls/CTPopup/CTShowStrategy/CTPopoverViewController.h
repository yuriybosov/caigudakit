//
//  MUPopoverViewController.h
//  CaigudaKit
//
//  Created by Malaar on 2/14/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTPopupView.h"

@interface CTPopoverViewController : UIViewController
{
    CTPopupView* popupedView;
}

@property (nonatomic, assign) UIPopoverController* popoverOwner;

- (id)initWithPopupView:(CTPopupView*)aPopupedView;

@end
