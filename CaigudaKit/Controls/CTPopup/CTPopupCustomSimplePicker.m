//
//  CTPopupCustomSimplePicker.m
//  Pods
//
//  Created by Malaar on 28.07.13.
//
//

#import "CTPopupCustomSimplePicker.h"
#import "CTTitledID.h"

@implementation CTPopupCustomSimplePicker

- (UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* result = nil;

    // configure label
    if(!view)
    {
        result = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 270, 44)];
        result.backgroundColor = [UIColor clearColor];
        
        if(self.font)
            result.font = self.font;
        if(self.textColor)
            result.textColor = self.textColor;
        if(self.shadowColor)
            result.shadowColor = self.shadowColor;
        result.shadowOffset = self.shadowOffset;
        result.textAlignment = self.textAlignment;
    }
    else
    {
        result = (UILabel*)view;
    }
    
    // setup text
    if(row < dataSource.count)
    {
        NSObject* item = [dataSource objectAtIndex:row];
        if([item isKindOfClass:[CTTitledID class]])
            result.text = ((CTTitledID*)item).title;
        else if([item isKindOfClass:[NSString class]])
            result.text = (NSString*)item;
        else if([item conformsToProtocol:@protocol(CTPopupPickerItemTitled)])
            result.text = ((NSObject<CTPopupPickerItemTitled>*)item).itemTitle;
        else
        {
            NSAssert(NO, @"Wrong class in dataSource !!!");
        }
    }
    else
    {
        result.text = nil;
    }

    return result;
}

@end
