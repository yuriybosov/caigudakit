//
//  MUPopupTimePicker.m
//  CaigudaKit
//
//  Created by Malaar on 9/29/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTPopupTimePicker.h"
#import "CTKitDefines.h"

@implementation CTPopupTimePicker

#pragma mark - override next methods to customize:

- (UIView*)createPicker
{
    int width = (CT_IS_IPAD) ? (260) : (320);
    UIDatePicker* pv = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, width, 216)];
    pv.datePickerMode = UIDatePickerModeTime;
    
    return pv;
}

@end
