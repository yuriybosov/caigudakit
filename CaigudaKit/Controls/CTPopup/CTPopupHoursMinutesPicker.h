//
//  MUPopupHoursMinutesPicker.h
//  CaigudaKit
//
//  Created by Malaar on 12/6/11.
//  Copyright (c) 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPopupPicker.h"

@interface CTPopupHoursMinutesPicker : CTPopupPicker <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, readonly) UIPickerView* popupedPicker;

@end
