//
//  MUSearchBar.m
//  CaigudaKit
//
//  Created by Malaar on 7/19/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTSearchBar.h"

@interface CTSearchBarDelegateHolder : NSObject <UISearchBarDelegate>

@property (nonatomic, weak) CTSearchBar* holdedSearchBar;

@end

@interface CTSearchBar()

- (void)setup;

@end

@implementation CTSearchBar

@synthesize validatableText, ctdelegate, filter;

#pragma mark - Init/Setup

- (id)init
{
    self = [super init];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

- (void)setup
{
    delegateHolder = [CTSearchBarDelegateHolder new];
    delegateHolder.self.holdedSearchBar = self;
    super.delegate = delegateHolder;
}

#pragma mark - CTValidationProtocol

- (void)setValidator:(CTValidator*)aValidator
{
    validator = aValidator;
    validator.validatableObject = self;
}

- (NSString*)validatableText
{
    return self.text;
}

- (void)setValidatableText:(NSString*)aValidatableText
{
    self.text = aValidatableText;
}

- (CTValidator*)validator
{
    return validator;
}

- (BOOL)validate
{
    return (validator) ? ([validator validate]) : (YES);
}

#pragma mark - CTFormatterProtocol

- (void)setFormatter:(CTFormatter*)aFormatter
{
    formatter = aFormatter;
    formatter.formattableObject = self;
}

- (CTFormatter*)formatter
{
    return formatter;
}

- (NSString *)formattingText
{
    return self.text;
}

#pragma mark - UISearchBarDelegate

- (void)setDelegate:(id<UISearchBarDelegate>)aDelegate
{
    ctdelegate = aDelegate;
}

- (id<UISearchBarDelegate>)delegate
{
    return ctdelegate;
}

@end


@implementation CTSearchBarDelegateHolder

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    BOOL result = YES;
    
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarShouldBeginEditing:)])
        result = [self.holdedSearchBar.ctdelegate searchBarShouldBeginEditing:searchBar];
    
    return result;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
//    [self.holdedSearchBar.keyboardAvoiding adjustOffset];
    
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarTextDidBeginEditing:)])
        [self.holdedSearchBar.ctdelegate searchBarTextDidBeginEditing:searchBar];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    BOOL result = YES;
    
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarShouldEndEditing:)])
        result = [self.holdedSearchBar.ctdelegate searchBarShouldEndEditing:searchBar];
    
    return result;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarTextDidEndEditing:)])
        [self.holdedSearchBar.ctdelegate searchBarTextDidEndEditing:searchBar];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBar:textDidChange:)])
        [self.holdedSearchBar.ctdelegate searchBar:searchBar textDidChange:searchText];
}

- (BOOL)searchBar:(CTSearchBar*)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL result = YES;
    if (searchBar.filter)
        result = [searchBar.filter textInField:searchBar shouldChangeCharactersInRange:range replacementString:text];
    
    if(result && [searchBar formatter])
        result = [[searchBar formatter] formatWithNewCharactersInRange:range replacementString:text];
    
    if(result && [self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBar:shouldChangeTextInRange:replacementText:)])
        result = [self.holdedSearchBar.ctdelegate searchBar:searchBar shouldChangeTextInRange:range replacementText:text];
    
    return result;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarSearchButtonClicked:)])
        [self.holdedSearchBar.ctdelegate searchBarSearchButtonClicked:searchBar];
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarBookmarkButtonClicked:)])
        [self.holdedSearchBar.ctdelegate searchBarBookmarkButtonClicked:searchBar];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarCancelButtonClicked:)])
        [self.holdedSearchBar.ctdelegate searchBarCancelButtonClicked:searchBar];
}

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBarResultsListButtonClicked:)])
        [self.holdedSearchBar.ctdelegate searchBarResultsListButtonClicked:searchBar];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    if([self.holdedSearchBar.ctdelegate respondsToSelector:@selector(searchBar:selectedScopeButtonIndexDidChange:)])
        [self.holdedSearchBar.ctdelegate searchBar:searchBar selectedScopeButtonIndexDidChange:selectedScope];
}

@end
