//
//  MUTextViewAutosize.h
//  CaigudaKit
//
//  Created by Malaar on 11/8/11.
//  Copyright (c) 2011 Caiguda. All rights reserved.
//

#import "CTTextView.h"

@interface CTTextViewAutosize : CTTextView

- (void)adjustSizeToContent;

@end
