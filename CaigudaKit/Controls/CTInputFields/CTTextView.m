//
//  MUTextView.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 9/6/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTTextView.h"
#import "CTKeyboardAvoidingProtocol.h"
#import "CTKitDefines.h"

@interface CTTextViewDelegateHolder : NSObject <UITextViewDelegate>

@property (nonatomic, weak) CTTextView* holdedTextView;

@end

@interface CTTextView ()

@property (nonatomic, retain) UITextView* placeholderTextView;

- (void)setup;
- (void)setPlaceHolderHidden:(BOOL)aHidden;

@end

@implementation CTTextView

@synthesize ctdelegate;
@synthesize keyboardAvoiding;
@synthesize observedText;
@synthesize filter;

@synthesize placeholder, placeholderColor, placeholderFont;

#pragma mark - Init

- (id)init
{
    if ((self = [super init]))
    {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
        [self setup];
    }
    return self;
}

#pragma mark - Setup

- (void)setup
{
    delegateHolder = [CTTextViewDelegateHolder new];
    delegateHolder.holdedTextView = self;
    
    placeholderColor = [UIColor grayColor];
    
    super.delegate = delegateHolder;
}

#pragma mark - CTValidationProtocol

- (void)setValidator:(CTValidator*)aValidator
{
    validator = aValidator;
    validator.validatableObject = self;
}

- (NSString*)validatableText
{
    return self.text;
}

- (void)setValidatableText:(NSString *)aValidatableText
{
    self.text = aValidatableText;
}

- (CTValidator*)validator
{
    return validator;
}

- (BOOL)validate
{
    return (validator) ? ([validator validate]) : (YES);
}

#pragma mark - Placeholder

- (UITextView*)placeholderTextView
{
    if (!_placeholderTextView)
    {
        _placeholderTextView = [[UITextView alloc] initWithFrame:self.bounds];
        _placeholderTextView.backgroundColor = [UIColor clearColor];
        _placeholderTextView.text = placeholder;
        _placeholderTextView.textColor = placeholderColor;
        _placeholderTextView.font = (placeholderFont) ? placeholderFont : self.font;
        _placeholderTextView.editable = NO;
        _placeholderTextView.userInteractionEnabled = NO;
        _placeholderTextView.hidden = [self.text length] > 0;
        _placeholderTextView.autoresizingMask = CTViewAutoresizingFlexibleSize;
        [self addSubview:_placeholderTextView];
    }
    return _placeholderTextView;
}

- (void)setPlaceholder:(NSString *)aPlaceholder
{
    placeholder = [aPlaceholder copy];
    self.placeholderTextView.text = placeholder;
}

- (void)setPlaceholderColor:(UIColor *)aPlaceholderColor
{
    placeholderColor = aPlaceholderColor;
    self.placeholderTextView.textColor = placeholderColor;
}

- (void)setPlaceholderFont:(UIFont *)aPlaceholderFont
{
    placeholderFont = aPlaceholderFont;
    self.placeholderTextView.font = placeholderFont;
}

- (void)setPlaceHolderHidden:(BOOL)aHidden
{
    self.placeholderTextView.hidden = aHidden;
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    if ([placeholder length])
        self.placeholderTextView.hidden = [self.text length] > 0;
}

- (void)setFont:(UIFont *)font
{
    [super setFont:font];
    if ([placeholder length] && !placeholderFont)
        self.placeholderTextView.font = font;
}


#pragma mark - UITextViewDelegate

- (void)setDelegate:(id<UITextViewDelegate>)delegate
{
    if(delegate)
    {
        NSAssert(NO, @"Must use mudelegate!");
    }
}

- (id<UITextViewDelegate>)delegate
{
    return delegateHolder;
}

@end


@implementation CTTextViewDelegateHolder

@synthesize holdedTextView;

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    BOOL result = YES;
    
    if([holdedTextView.ctdelegate respondsToSelector:@selector(textViewShouldBeginEditing:)])
        result = [holdedTextView.ctdelegate textViewShouldBeginEditing:textView];
    
    return result;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    BOOL result = YES;
    
    if([holdedTextView.ctdelegate respondsToSelector:@selector(textViewShouldEndEditing:)])
        result = [holdedTextView.ctdelegate textViewShouldEndEditing:textView];
    
    return result;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [holdedTextView.keyboardAvoiding adjustOffset];
    
    if([holdedTextView.ctdelegate respondsToSelector:@selector(textViewDidBeginEditing:)])
        [holdedTextView.ctdelegate textViewDidBeginEditing:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    holdedTextView.observedText = textView.text;
    
    if([holdedTextView.ctdelegate respondsToSelector:@selector(textViewDidEndEditing:)])
        [holdedTextView.ctdelegate textViewDidEndEditing:textView];
}

- (BOOL)textView:(CTTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL result = YES;
    
    if (textView.filter)
        result = [textView.filter textInField:textView shouldChangeCharactersInRange:range replacementString:text];
    
    if(result && [holdedTextView.ctdelegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)])
        result = [holdedTextView.ctdelegate textView:textView shouldChangeTextInRange:range replacementText:text];
    
    if(result)
    {
        NSString* newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
        [textView setPlaceHolderHidden:newString.length];
    }
    
    return result;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if([holdedTextView.ctdelegate respondsToSelector:@selector(textViewDidChange:)])
        [holdedTextView.ctdelegate textViewDidChange:textView];
}

- (void)textViewDidChangeSelection:(UITextView *)textView
{
    if([holdedTextView.ctdelegate respondsToSelector:@selector(textViewDidChangeSelection:)])
        [holdedTextView.ctdelegate textViewDidChangeSelection:textView];
}


@end