//
//  MUTextField.h
//  CaigudaKit
//
//  Created by Malaar on 8/19/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTValidator.h"
#import "CTFilter.h"
#import "CTFormatter.h"
#import "CTKeyboardAvoiderProtocol.h"

@class CTTextFieldDelegateHolder;

@interface CTTextField : UITextField <UITextFieldDelegate, CTValidationProtocol, CTFormatterProtocol, CTKeyboardAvoiderProtocol>
{
    CTValidator* validator;
    CTFormatter* formatter;
    CTTextFieldDelegateHolder* delegateHolder;
}

@property (nonatomic, weak) id<UITextFieldDelegate> ctdelegate;
@property (nonatomic, retain) CTFilter *filter;
@property (nonatomic, assign) CGFloat padding;
@property (nonatomic, assign) BOOL hidenCursor;

@end
