//
//  MUTextViewAutosize.m
//  CaigudaKit
//
//  Created by Malaar on 11/8/11.
//  Copyright (c) 2011 Caiguda. All rights reserved.
//

#import "CTTextViewAutosize.h"

@implementation CTTextViewAutosize

- (void)setText:(NSString *)aText
{
    [super setText:aText];
    [self adjustSizeToContent];
}

- (void)adjustSizeToContent
{
    CGRect frame = self.frame;
    frame.size.height = self.contentSize.height;
    self.frame = frame;
}

@end
