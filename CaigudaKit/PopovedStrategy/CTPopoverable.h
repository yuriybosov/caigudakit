//
//  MDPopovable.h
//  CaigudaKit
//
//  Created by Malaar on 07.06.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CTPopoverable <NSObject>

@property (nonatomic, assign) UIPopoverController* ct_popover;

@end

