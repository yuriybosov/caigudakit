//
//  CTActivityHUD.h
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTActivityAdapter.h"
#import "MBProgressHUD.h"

/**
 * This adapter provide wrapper around MBProgressHUD activity.
 * @see CTActivityAdapter
 */
@interface CTActivityHUD : CTActivityAdapter
{
    MBProgressHUD* activity;
}

@end
