//
//  CTPullToRefreshStrategy.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTPullToRefreshAdapter.h"

@implementation CTPullToRefreshAdapter

- (void)configureWithTableView:(UITableView*)aTableView
{
    // override it in subclasses
}

- (void)releaseRefreshControl
{
    // override it in subclasses
}

- (void)beginPullToRefresh
{
    self.refreshCallback(self);
}

- (void)endPullToRefresh
{
    // override it in subclasses
}

@end
