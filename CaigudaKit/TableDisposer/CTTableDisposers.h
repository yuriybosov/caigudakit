//
//  CTTableDisposers.h
//  CaigudaKit
//
//  Created by Malaar on 05.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#ifndef CaigudaKit_CTTableDisposers_h
#define CaigudaKit_CTTableDisposers_h

// cell datas (base)
#import "CTCellData.h"
#import "CTCellDataMaped.h"
#import "CTCellDataModeled.h"

// cell datas (simple)
#import "CTCellDataButton.h"
#import "CTCellDataLabel.h"
#import "CTCellDataStandart.h"
#import "CTCellDataSwitch.h"
#import "CTCellDataTextField.h"
#import "CTCellDataTextView.h"

// cells
#import "CTCell.h"
#import "CTCellButton.h"
#import "CTCellLabel.h"
#import "CTCellStandart.h"
#import "CTCellSwitch.h"
#import "CTCellTextField.h"
#import "CTCellTextView.h"

// sections
#import "CTSectionReadonly.h"
#import "CTSectionWritable.h"

// table disposers
#import "CTTableDisposer.h"
#import "CTTableDisposerMapped.h"
#import "CTTableDisposerModeled.h"

#endif
