//
//  CTTableDisposer.h
//  CaigudaKit
//
//  Created by Malaar on 30.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTSectionWritable.h"
#import "CTCellData.h"
#import "CTMulticastDelegate.h"

@protocol CTTableDisposerDelegate;
@protocol CTTableDisposerMulticastDelegate;

@interface CTTableDisposer : NSObject <UITableViewDataSource, UITableViewDelegate, NSCopying>
{
@protected
    UITableView* tableView;
    NSMutableArray* sections;
}

@property (nonatomic, assign) Class tableClass;
@property (nonatomic, assign) UITableViewStyle tableStyle;
@property (nonatomic, readonly) UITableView* tableView;
@property (nonatomic, weak) id<CTTableDisposerDelegate> delegate;
@property (nonatomic, readonly) CTMulticastDelegate<CTTableDisposerMulticastDelegate>* multicastDelegate;

- (void)setupTableView:(UITableView *)aTableView;

/**
 * Use it in viewDidUnload
 **/
- (void)releaseView;

#pragma mark - Multicast Delegates
- (void)addDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;
- (void)removeDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;

#pragma mark - Sections
- (void)addSection:(CTSectionReadonly*)aSection;
- (void)insertSection:(CTSectionReadonly*)aSection atIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)removeSectionAtIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)removeSection:(CTSectionReadonly*)aSection needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)removeAllSections;
- (CTSectionReadonly*)sectionByIndex:(NSUInteger)anIndex;
- (NSUInteger)indexBySection:(CTSectionReadonly*)aSection;
- (NSUInteger)sectionsCount;

#pragma mark - Search CellData & IndexPath
- (NSIndexPath*)indexPathByCellData:(CTCellData*)aCellData;
- (NSIndexPath*)indexPathByVisibleCellData:(CTCellData*)aCellData;
- (CTCellData*)cellDataByIndexPath:(NSIndexPath*)anIndexPath;
- (CTCellData*)visibleCellDataByIndexPath:(NSIndexPath*)anIndexPath;
- (CTCellData*)cellDataByTag:(NSUInteger)aTag;

#pragma mark - Show/Hide cellData
- (void)showCellByIndexPath:(NSIndexPath*)anIndexPath needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)hideCellByIndexPath:(NSIndexPath*)anIndexPath needUpdateTable:(BOOL)aNeedUpdateTable;

- (void)showCellByIndexPath:(NSIndexPath*)anIndexPath
            needUpdateTable:(BOOL)aNeedUpdateTable
           withRowAnimation:(UITableViewRowAnimation)aTableViewRowAnimation;

- (void)hideCellByIndexPath:(NSIndexPath*)anIndexPath
            needUpdateTable:(BOOL)aNeedUpdateTable
           withRowAnimation:(UITableViewRowAnimation)aTableViewRowAnimation;


#pragma mark - Deletions
- (void)deleteRowsAtIndexPaths:(NSArray*)anIndexPaths withRowAnimation:(UITableViewRowAnimation)aTableViewRowAnimation;

#pragma mark - Data reloading
- (void)reloadData;
- (void)reloadSectionsWithAnimation:(UITableViewRowAnimation)anAnimation;

#pragma mark - 
- (void)scrollToBottom:(BOOL)anAnimated;

#pragma mark -
- (void)didCreateCell:(CTCell*)aCell;

@end



@protocol CTTableDisposerDelegate <UITableViewDelegate>

@optional

- (BOOL)tableView:(UITableView *)aTableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)tableView:(UITableView *)aTableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)aTableView;
- (NSInteger)tableView:(UITableView *)aTableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index;
- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)aTableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;

- (void)tableDisposer:(CTTableDisposer*)aTableDisposer didCreateCell:(CTCell*)aCell;

@end


@protocol CTTableDisposerMulticastDelegate <NSObject>

@optional
- (void)tableDisposer:(CTTableDisposer*)aTableDisposer didCreateCell:(CTCell*)aCell;
- (void)tableView:(UITableView *)aTableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;

@end
