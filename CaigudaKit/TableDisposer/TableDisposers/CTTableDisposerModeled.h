//
//  CTTableDisposerModeled.h
//
//
//  Created by Malaar on 30.03.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTTableDisposer.h"
#import "CTCellDataModeled.h"

@protocol CTTableDisposerModeledDelegate;
@protocol CTTableDisposerModeledMulticastDelegate;

@interface CTTableDisposerModeled : CTTableDisposer
{
    NSMutableDictionary* registeredClasses;
    
    Class compoundCellDataClass;
    Class compoundModelClass;
}

@property (nonatomic, weak) id<CTTableDisposerModeledDelegate> modeledDelegate;
@property (nonatomic, readonly) CTMulticastDelegate<CTTableDisposerModeledMulticastDelegate>* modeledMulticastDelegate;

@property (nonatomic, assign) BOOL useCompoundCells;
@property (nonatomic, assign) Class compoundCellDataClass;
@property (nonatomic, assign) Class compoundModelClass;

- (void)registerCellData:(Class)aCellDataClass forModel:(Class)aModelClass;
- (void)unregisterCellDataForModel:(Class)aModelClass;

- (void)setupModels:(NSArray*)aModels forSectionAtIndex:(NSUInteger)aSectionIndex;
- (void)setupModels:(NSArray*)aModels forSection:(CTSectionReadonly*)aSection;

- (CTCellDataModeled*)cellDataFromModel:(id)aModel;

#pragma mark - Modeled multicast delegates
- (void)addModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;
- (void)removeModeledDelegate:(id)aDelegate delegateQueue:(dispatch_queue_t)aDelegateQueue;

#pragma mark -
- (void)didCreateCellData:(CTCellData*)aCellData;

@end


@protocol CTTableDisposerModeledDelegate <NSObject>

@optional
- (void)tableDisposer:(CTTableDisposerModeled*)aTableDisposer didCreateCellData:(CTCellData*)aCellData;
- (Class)tableDisposer:(CTTableDisposerModeled*)aTableDisposer cellDataClassForUnregisteredModel:(id)aModel;

@end


@protocol CTTableDisposerModeledMulticastDelegate <NSObject>

@optional
- (void)tableDisposer:(CTTableDisposerModeled*)aTableDisposer didCreateCellData:(CTCellData*)aCellData;

@end
