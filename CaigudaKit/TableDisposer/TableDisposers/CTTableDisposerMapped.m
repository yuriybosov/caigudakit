//
//  CTTableDisposerMaped.m
//  CaigudaKit
//
//  Created by Malaar on 30.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTTableDisposerMapped.h"

#import "CTCellDataMaped.h"
#import "CTKeyboardAvoidingTableView.h"

@interface CTTableDisposerMapped ()

- (void)mapFromObject;
@end

@implementation CTTableDisposerMapped

- (void)mapFromObject
{
    for(CTSectionReadonly* section in sections)
    {
        if([section isKindOfClass:[CTSectionWritable class]])
        {
            [(CTSectionWritable*)section mapFromObject];
        }
    }
}

- (void)mapToObject
{
    for(CTSectionReadonly* section in sections)
    {
        if([section isKindOfClass:[CTSectionWritable class]])
        {
            [(CTSectionWritable*)section mapToObject];
        }
    }
}

- (void)reloadData
{
    if([tableView isKindOfClass:[CTKeyboardAvoidingTableView class]])
    {
        [((CTKeyboardAvoidingTableView*)tableView) removeAllObjectsForKeyboard];
    }
    
    [self mapFromObject];
    
    for(CTSectionReadonly* section in sections)
    {
        [section updateCellDataVisibility];
    }
    [tableView reloadData];
}

@end
