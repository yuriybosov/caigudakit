//
//  MULabelCell.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellStandart.h"
#import "CTCellDataStandart.h"
#import "UIImageView+AFNetworking.h"

@implementation CTCellStandart

- (void)setupCellData:(CTCellData *)aCellData
{
    [super setupCellData:aCellData];

    CTCellDataStandart *cellDataStandart = (CTCellDataStandart*)self.cellData;
    
    self.textLabel.text = cellDataStandart.title;
    self.textLabel.font = cellDataStandart.titleFont;
    self.textLabel.textColor = cellDataStandart.titleColor;
    self.textLabel.textAlignment = cellDataStandart.titleTextAlignment;
    
    self.detailTextLabel.textColor = cellDataStandart.subtitleColor;
    self.detailTextLabel.font = cellDataStandart.subtitleFont;
    self.detailTextLabel.text = cellDataStandart.subtitle;
    
    if(cellDataStandart.imageURL)
        [self.imageView setImageWithURL:cellDataStandart.imageURL placeholderImage:cellDataStandart.imagePlaceholder];
    else
        [self.imageView setImage:cellDataStandart.image];
}

@end
