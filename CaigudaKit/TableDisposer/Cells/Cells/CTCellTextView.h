//
//  MUCellTextView.h
//  CaigudaKit
//
//  Created by Malaar on 04.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCell.h"
#import "CTTextView.h"


@interface CTCellTextView : CTCell
{
    UILabel* titleLabel;
}

@property (nonatomic, readonly) CTTextView* textView;

@end
