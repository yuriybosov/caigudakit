//
//  MULabelCellData.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataStandart.h"
#import "CTCellStandart.h"


@implementation CTCellDataStandart

@synthesize subtitle;
@synthesize image;
@synthesize imageURL;
@synthesize imagePlaceholder;
@synthesize title;
@synthesize titleFont;
@synthesize subtitleFont;
@synthesize titleColor;
@synthesize subtitleColor;
@synthesize titleTextAlignment;

#pragma mark - Init/Dealloc

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super initWithObject:aObject key:aKey];
    if (self)
    {
        self.cellClass = [CTCellStandart class];
        
        titleTextAlignment = NSTextAlignmentLeft;
        titleFont = [UIFont systemFontOfSize:18];
        subtitleFont = [UIFont systemFontOfSize:16];
        titleColor = [UIColor blackColor];
        subtitleColor = [UIColor grayColor];
    }
    return self;
}

#pragma mark - Maping

- (void)mapFromObject
{
    if (object && key)
        subtitle = [object valueForKeyPath:key];
}

- (void)mapToObject
{
    if (object && key)
        [object setValue:subtitle forKeyPath:key];
}

@end
