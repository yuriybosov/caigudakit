//
//  MUCellDataTextPair.h
//  CaigudaKit
//
//  Created by Malaar on 04.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataMaped.h"

@interface CTCellDataTextPair : CTCellDataMaped

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) UIColor *titleColor;
@property (nonatomic, strong) UIFont *titleFont;

@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) UIColor *textColor;
@property (nonatomic, strong) UIFont *textFont;

@end
