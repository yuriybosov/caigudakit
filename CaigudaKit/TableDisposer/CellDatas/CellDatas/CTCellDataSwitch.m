//
//  MUBooleanCellData.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataSwitch.h"
#import "CTCellSwitch.h"

@implementation CTCellDataSwitch

@synthesize boolValue;
@synthesize targetAction;
@synthesize onText;
@synthesize offText;

#pragma mark - Init/Dealloc

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super initWithObject:aObject key:aKey];
    if(self)
    {
        self.cellClass = [CTCellSwitch class];
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
        targetAction = [[CTTargetAction alloc] init];
    }
    return self;
}

#pragma mark - Target/Action

- (void)setTarget:(id)aTarget action:(SEL)anAction
{
    [targetAction setTarget:aTarget action:anAction];
}

#pragma mark - Maping

- (void)mapFromObject
{
    if (object && key)
        boolValue = [[object valueForKeyPath:key] boolValue];
}

- (void)mapToObject
{
    if (object && key)
        [object setValue:[NSNumber numberWithBool:boolValue] forKeyPath:key];
}

@end
