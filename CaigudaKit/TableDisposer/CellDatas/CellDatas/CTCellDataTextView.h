//
//  MUCellDataTextView.h
//  CaigudaKit
//
//  Created by Malaar on 04.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataTextPair.h"
#import "CTValidator.h"
#import "CTFilter.h"


@interface CTCellDataTextView : CTCellDataTextPair

@property (nonatomic, assign) NSTextAlignment textAlignment;

@property (nonatomic, assign) UITextAutocapitalizationType autocapitalizationType;   ///< default is UITextAutocapitalizationTypeSentences
@property (nonatomic, assign) UITextAutocorrectionType autocorrectionType;           ///< default is UITextAutocorrectionTypeDefault
@property (nonatomic, assign) UIKeyboardType keyboardType;                           ///< default is UIKeyboardTypeDefault
@property (nonatomic, assign) UIKeyboardAppearance keyboardAppearance;               ///< default is UIKeyboardAppearanceDefault
@property (nonatomic, assign) UIReturnKeyType returnKeyType;                         ///< default is UIReturnKeyDefault

@property (nonatomic, retain) CTValidator *validator;
@property (nonatomic, retain) CTFilter *filter;

@property (nonatomic, retain) NSString* placeholder;
@property (nonatomic, retain) UIColor* placeholderColor;

@end
