//
//  MUCellData.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTCell;
@class CTSectionReadonly;

typedef enum _CTCellSeparatorStyle
{
    CTCellSeparatorStyleNone = 0,
    CTCellSeparatorStyleTop = 1 << 0,
    CTCellSeparatorStyleBottom = 1 << 1,
} CTCellSeparatorStyle;

@interface CTCellData : NSObject
{
    @private
    NSMutableArray *cellSelectedHandlers;
    NSMutableArray *cellDeselectedHandler;
}

@property (nonatomic, retain) NSString* cellNibName;

@property (nonatomic, assign) Class cellClass;
@property (nonatomic, readonly) NSString* cellIdentifier;

@property (nonatomic, assign) UITableViewCellStyle cellStyle;
@property (nonatomic, assign) UITableViewCellSelectionStyle cellSelectionStyle;
@property (nonatomic, assign) UITableViewCellAccessoryType cellAccessoryType;
@property (nonatomic, assign) BOOL autoDeselect;
@property (nonatomic, assign) BOOL visible;
@property (nonatomic, assign) BOOL enableEdit;
@property (nonatomic, assign) BOOL disableInputTraits;
@property (nonatomic, assign) BOOL cellHeightAutomaticDimension;
@property (nonatomic, assign) NSUInteger tag;
@property (nonatomic, strong) NSDictionary* userData;

@property (nonatomic, assign) CGFloat cellHeight;
@property (nonatomic, assign) CGFloat cellWidth;

@property (nonatomic, assign) CTCellSeparatorStyle cellSeparatorStyle;  // by default is CTCellSeparatorStyleNone
@property (nonatomic, retain) UIColor* cellTopSeparatorColor;           // by default is nil
@property (nonatomic, retain) UIColor* cellBottomSeparatorColor;        // by default is nil

//@property (nonatomic, weak) UITableView* tableView;
@property (nonatomic, weak) CTSectionReadonly* section;

- (id)init;
- (CGFloat)cellHeightForWidth:(CGFloat) aWidth;

- (void)addCellSelectedTarget:(id)aTarget action:(SEL)anAction;
- (void)addCellDeselectedTarget:(id)aTarget action:(SEL)anAction;

- (void)performSelectedHandlers;
- (void)performDeselectedHandlers;

- (CTCell*)createCell;

@end
