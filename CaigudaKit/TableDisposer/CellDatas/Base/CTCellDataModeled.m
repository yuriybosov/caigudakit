//
//  MUCellDataModeled.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataModeled.h"

@implementation CTCellDataModeled

@synthesize model;

- (id) init
{
    NSAssert(NO, @"You can't use this method! Instead use 'initWithModel:'");
    return nil;
}

- (id)initWithModel:(id)aModel
{
    self = [super init];
    if (self)
    {
        model = aModel;
    }
    return self;
}

@end
