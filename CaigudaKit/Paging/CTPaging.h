//
//  CTPaging.h
//  CaigudaKit
//
//  Created by Malaar on 19.02.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPagingConfig.h"
#import "CTCompoundCell.h"
#import "CTGateway.h"
#import "CTFetchable.h"
#import "CTTableDisposerModeled.h"
#import "CTPagingMoreCellProtocols.h"

@protocol CTPagingDelegate;

@interface CTPaging : NSObject <CTFetchable, CTTableDisposerMulticastDelegate, CTTableDisposerModeledMulticastDelegate>
{
    Reachability* reachability;
    CTCell<CTPagingMoreCellProtocol>* moreCell;
}

@property (nonatomic, readonly) CTPagingConfig* pagingConfig;
@property (nonatomic, readonly) CTTableDisposerModeled* tableDisposer;

@property (nonatomic, weak) id<CTPagingDelegate> delegate;

@property (nonatomic, assign) BOOL reloading;
@property (nonatomic, assign) BOOL loadingMore;
@property (nonatomic, strong) NSDate* lastUpdate;

@property (nonatomic, readonly) NSUInteger pageOffset;
@property (nonatomic, readonly) NSMutableArray* models;
@property (nonatomic, readonly) NSMutableArray* compoundModels;

#pragma mark - Init/Dealloc
- (id)initWithConfig:(CTPagingConfig*)aPagingConfig
       tableDisposer:(CTTableDisposerModeled*)aTableDisposer;

#pragma mark - Load/Reload data
- (void)reloadData;
- (void)loadMoreData;
- (NSArray*)didFetchData:(NSArray*)aData;
- (void)setupModels:(NSArray*)aModels;

#pragma mark - Notifications
- (void)reachabilityChangedNotification:(NSNotification*)aNotification;

@end


@protocol CTPagingDelegate <NSObject>

- (CTGatewayRequest*)fetchRequestForPaging:(CTPaging*)aPaging;
- (CTCellData<CTPagingMoreCellDataProtocol>*)moreCellDataForPaging:(CTPaging*)aPaging;

@optional

- (BOOL)shouldFetchForPaging:(CTPaging*)aPaging;
- (void)willBeginFetchingForPaging:(CTPaging*)aPaging;
- (void)didCompleteFetchingForPaging:(CTPaging*)aPaging;

- (NSArray*)paging:(CTPaging*)aPaging didFetchedDataWithSuccess:(NSArray*)aData;
- (void)paging:(CTPaging*)aPaging didFetchedDataWithFailure:(CTResponse*)aResponse;

@end


