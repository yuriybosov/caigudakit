//
//  CTPagingConfig.m
//  CaigudaKit
//
//  Created by Malaar on 19.02.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTPagingConfig.h"

@implementation CTPagingConfig

- (id)init
{
    self = [super init];
    if(self)
    {
        _pageSize = 20;
        _enablePaging = YES;
        _loadMoreDataType = CTLoadMoreDataTypeAuto;
        _useCompoundCells = NO;
        _compoundCellsGroupByCount = 2;
    }
    return self;
}

@end
