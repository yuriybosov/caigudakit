//
//  CTModuleListPaging.m
//  OutSpeak
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleListPaging.h"
#import "CTModuleListPaging+Private.h"
#import "CTCompoundModel.h"

@implementation CTModuleListPaging

@synthesize itemsAsPage;
@synthesize initialPageOffset;
@synthesize pageSize, pageOffset;
@synthesize loadMoreDataType;

@synthesize reloading, loadingMore;

- (id)initWithTableDisposer:(CTTableDisposerModeled *)aTableDisposer
{
    return [self initWithTableDisposer:aTableDisposer initialPageOffset:0 itemsAsPage:NO];
}

- (id)initWithTableDisposer:(CTTableDisposerModeled *)aTableDisposer
          initialPageOffset:(NSUInteger)anInitialPageOffset
                itemsAsPage:(BOOL)anItemsAsPage
{
    self = [super initWithTableDisposer:aTableDisposer];
    if(self)
    {
        itemsAsPage = anItemsAsPage;
        initialPageOffset = anInitialPageOffset;
        
        pageSize = 20;
        loadMoreDataType = CTLoadMoreDataTypeAuto;
        pageOffset = initialPageOffset;

        models = [NSMutableArray new];
        compoundModels = [NSMutableArray new];
        [defaultTableDisposer addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return self;
}

- (void)releaseViews
{
    moreCell = nil;
    [super releaseViews];
}

- (void)setDelegate:(id<CTModuleListDelegate>)aDelegate
{
    NSAssert([aDelegate conformsToProtocol:@protocol(CTModuleListPagingDelegate)],
             @"CTModuleListPaging delegate should conform CTModuleListPagingDelegate protocol");
    delegate = aDelegate;
}

#pragma mark - Reload

- (void)reloadData
{
    CTFetcherMessagePaging *nextMessage = (CTFetcherMessagePaging*)[self fetcherMessage];
    nextMessage.pagingOffset = initialPageOffset;
    nextMessage.reloading = YES;
    nextMessage.loadingMore = NO;
    
//    CTLog(@"---reloaddata---, %d  %d", reloading, loadingMore);
    
    if([self.dataFetcher canFetchWithMessage:nextMessage])
    {
//        CTLog(@"---reloaddata---, %d  %d", reloading, loadingMore);
        pageOffset = nextMessage.pagingOffset;
        reloading = nextMessage.reloading;
        loadingMore = nextMessage.loadingMore;
//        CTLog(@"---reloaddata---, %d  %d", reloading, loadingMore);
    }
    
    [self fetchDataWithMessage:nextMessage];
//    CTLog(@"---reloaddata---, %d  %d", reloading, loadingMore);
}

#pragma mark - Load more

- (void)loadMoreData
{
    if (reloading)
        return;
    
    if(itemsAsPage)
        pageOffset += 1;
    else
        pageOffset += pageSize;
    
    loadingMore = YES;

    [self fetchDataWithMessage:[self fetcherMessage]];
}

- (void)willFetchDataWithMessage:(CTFetcherMessagePaging *)aMessage
{
    if(aMessage.loadingMore)
        [moreCell didBeginDataLoading];
    else
        [self.activityAdapter showActivity:YES];
    
    if([self.delegate respondsToSelector:@selector(willFetchDataForModuleList:)])
        [self.delegate willFetchDataForModuleList:self];
}

- (void)didFetchDataWithMessage:(CTFetcherMessagePaging *)aMessage andResponse:(CTResponse *)aResponse
{
    [super didFetchDataWithMessage:aMessage andResponse:aResponse];
    
    BOOL success = aResponse.success;
//    CTLog(@"did fetch modulelistpaging with success----- %d --- cancelled -- %d reloading -- %d loadingmore -- %d", success, aResponse.isRequestCancelled, aMessage.reloading, aMessage.loadingMore);
    
    if(success)
    {
        if(aMessage.reloading)
        {
//            CTLog(@"remove all objects from models with count -- %d", models.count);
            [models removeAllObjects];
            [compoundModels removeAllObjects];
        }
        else if (aMessage.loadingMore)
        {
            [moreCell didEndDataLoading];
        }
    }
    
    if (aMessage.reloading)
        reloading = NO;
    else if (aMessage.loadingMore)
        loadingMore = NO;
}

#pragma mark - Message

- (Class)fetcherMessageClass
{
    return [CTFetcherMessagePaging class];
}

- (void)configureFetcherMessage:(CTFetcherMessagePaging*)aMessage
{
    [super configureFetcherMessage:aMessage];
    aMessage.pagingOffset = pageOffset;
    aMessage.pagingSize = pageSize;
    aMessage.reloading = reloading;
    aMessage.loadingMore = loadingMore;
}

#pragma mark - Setup

- (void)setupModels:(NSArray*)aModels forSection:(CTSectionReadonly *)aSection
{
//    [models addObjectsFromArray:aModels];
    if (self.tableDisposer.useCompoundCells)
        [compoundModels addObjectsFromArray:[CTCompoundModel compoundModelsFromModels:aModels
                                                                       groupedByCount:self.defaultCompoundMaxModelsCount]];

    [self.tableDisposer setupModels:(self.tableDisposer.useCompoundCells) ? (compoundModels) : (models)
                         forSection:aSection];

    [self setupMoreCellDataForSection:aSection withPagedModelsCount:aModels.count];
//    [self.tableDisposer reloadData];
}

- (void)saveModels:(NSArray*)aModels
{
    [models addObjectsFromArray:aModels];
}

- (void)setupMoreCellDataForSection:(CTSectionReadonly*)section withPagedModelsCount:(NSUInteger)modelsCount
{
    moreCell = nil;
    if ((modelsCount == self.pageSize && self.pageSize) && section && [self.delegate respondsToSelector:@selector(moreCellDataForPagingModule:)])
    {
        CTCellData<CTPagingMoreCellDataProtocol>* moreCellData = [(id<CTModuleListPagingDelegate>)self.delegate moreCellDataForPagingModule:self];
        if (moreCellData)
        {
            if ([moreCellData respondsToSelector:@selector(addTarget:action:)])
                [moreCellData addTarget:self action:@selector(loadMoreDataPressed:)];
            [section addCellData:moreCellData];
        }
    }
}

#pragma mark - CTTableDisposerMulticastDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(self.tableDisposer.tableView != tableView)
        return;
    
    if([cell conformsToProtocol:@protocol(CTPagingMoreCellProtocol)])
    {
        moreCell = (CTCell<CTPagingMoreCellProtocol>*)cell;
        if(loadMoreDataType == CTLoadMoreDataTypeAuto)
            [self loadMoreData];
    }
}

#pragma mark - Actions

- (void)loadMoreDataPressed:(id)aSender
{
    [self loadMoreData];
}

@end
