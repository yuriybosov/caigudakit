//
//  CTModuleList.m
//  OutSpeak
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleList.h"
#import "CTCompoundModel.h"
#import "CTKitDefines.h"

@implementation CTModuleList

@synthesize delegate, lastUpdateDate,delegateSearch;
@synthesize pullToRefreshAdapter;
@synthesize moduleQueue;
@synthesize isSearchEnabled,minSearchStringLength;
@dynamic models;
@dynamic compoundModels;

#pragma mark - Init/Dealloc

- (id)initWithTableDisposer:(CTTableDisposerModeled*)aTableDisposer
{
    self = [super init];
    if(self)
    {
        defaultTableDisposer = aTableDisposer;
        [defaultTableDisposer addModeledDelegate:self delegateQueue:dispatch_get_main_queue()];
        moduleQueue = dispatch_queue_create("com.caiguda.CTModuleList", NULL);
        minSearchStringLength = 0;
    }
    return self;
}

- (void)dealloc
{
    [self.dataFetcher cancelFetching];
#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
    if (moduleQueue)
        dispatch_release(moduleQueue);
#endif
    moduleQueue = NULL;
}

#pragma mark - Properties

- (CTTableDisposerModeled *)tableDisposer
{
    if (self.isSearchEnabled && [self.searchStrategy isSearchActive])
        return self.searchStrategy.tableDisposer;
    
    return defaultTableDisposer;
}

- (void)setDataFetcher:(id<CTDataFetcher>)dataFetcher
{
    _dataFetcher = dataFetcher;
    _dataFetcher.callbackQueue = moduleQueue;
}

- (void)setSearchStrategy:(CTSearchListStrategy *)searchStrategy
{
    _searchStrategy = searchStrategy;
    _searchStrategy.delegate = self;
}

- (void)setPullToRefreshAdapter:(CTPullToRefreshAdapter *)aPullToRefreshAdapter
{
    __weak CTModuleList* __self = self;
    pullToRefreshAdapter = aPullToRefreshAdapter;
    pullToRefreshAdapter.refreshCallback = ^(CTPullToRefreshAdapter* aPullToRefreshAdapter)
    {
        [__self reloadData];
    };
}

- (NSArray *)models
{
    return [NSArray arrayWithArray:models];
}

- (NSArray *)compoundModels
{
    return [NSArray arrayWithArray:compoundModels];
}

#pragma mark - Views

- (void)configureWithView:(UIView*)aView
{
    [pullToRefreshAdapter configureWithTableView:defaultTableDisposer.tableView];
    [_activityAdapter configureWithView:aView];
}

- (void)releaseViews
{
    [pullToRefreshAdapter releaseRefreshControl];
    [_activityAdapter releaseActivityView];
    if (self.isSearchEnabled)
        [self.searchStrategy releaseViews];
}

#pragma mark - Data fetching

- (void)fetchDataWithMessage:(CTFetcherMessage *)aMessage
{
    if([self.dataFetcher canFetchWithMessage:aMessage])
    {
        [self.dataFetcher cancelFetching];
        [self willFetchDataWithMessage:aMessage];
        
        __weak CTModuleList* __self = self;
        [self.dataFetcher fetchDataByMessage:aMessage withCallback:^(CTResponse *aResponse)
         {
             dispatch_async(dispatch_get_main_queue(), ^
               {
                   [__self didFetchDataWithMessage:aMessage andResponse:aResponse];
                   if(aResponse.success)
                   {
                       NSArray* aModels = [__self processFetchedModelsInResponse:aResponse];

                       [__self prepareSectionsForModels:aModels];
                       CTSectionReadonly* section;
                       if([aModels.firstObject isKindOfClass:[NSArray class]])
                       {
                           [self saveModels:aModels];
                           for(NSUInteger i = 0; i < aModels.count; ++i)
                           {
                               section = [__self.tableDisposer sectionByIndex:i];
                               [__self setupModels:aModels[i] forSection:section];
                           }
                       }
                       else
                       {
                           [self saveModels:aModels];
                           section = [__self.tableDisposer sectionByIndex:0];
                           [__self setupModels:aModels forSection:section];
                       }

                       [self.tableDisposer reloadData];
                       
                       if([__self.delegate respondsToSelector:@selector(moduleList:didReloadDataWithModels:)])
                           [__self.delegate moduleList:__self didReloadDataWithModels:aModels];
                   }
                   else if(!aResponse.requestCancelled)
                   {
                       if(__self.fetcherFailedCallback)
                           __self.fetcherFailedCallback(__self, aResponse);
                   }
               });
         }];
    }
    else
    {
        [self.pullToRefreshAdapter endPullToRefresh];

        if(self.fetcherCantFetchCallback)
            self.fetcherCantFetchCallback(self, aMessage);
    }
}

- (void)willFetchDataWithMessage:(CTFetcherMessage *)aMessage
{
    [self.activityAdapter showActivity:YES];
    if([self.delegate respondsToSelector:@selector(willFetchDataForModuleList:)])
        [self.delegate willFetchDataForModuleList:self];
}

- (void)didFetchDataWithMessage:(CTFetcherMessage *)aMessage andResponse:(CTResponse *)aResponse
{
    [self.activityAdapter hideActivity:YES];
    [self.pullToRefreshAdapter endPullToRefresh];
    
    if(aResponse.success)
        lastUpdateDate = [NSDate date];

    if([self.delegate respondsToSelector:@selector(didFetchDataForModuleList:)])
        [self.delegate didFetchDataForModuleList:self];
}

- (void)reloadData
{
    models = nil;
    [self fetchDataWithMessage:[self fetcherMessage]];
}

#pragma mark - Message

- (Class)fetcherMessageClass
{
    return [CTFetcherMessage class];
}

- (CTFetcherMessage*)createFetcherMessage
{
    CTFetcherMessage* message = nil;
    if([self.delegate respondsToSelector:@selector(fetcherMessageForModuleList:)])
        message = [self.delegate fetcherMessageForModuleList:self];

    if(message)
    {
        if(![message isKindOfClass:self.fetcherMessageClass])
        {
            NSAssert(NO, @"Wrong fetcher message class!");
            return nil;
        }
    }
    else
        message = [self.fetcherMessageClass new];

    return message;
}

- (void)configureFetcherMessage:(CTFetcherMessage*)aMessage
{
    if (self.isSearchEnabled && [self.searchStrategy isSearchActive])
        aMessage.defaultParameters[kCTSearchListStrategySearchTextKey] = self.searchStrategy.currentSearchText;
}

- (CTFetcherMessage*)fetcherMessage
{
    CTFetcherMessage* message = [self createFetcherMessage];
    [self configureFetcherMessage:message];
    return message;
}

#pragma mark - Process Models

- (NSArray*)processFetchedModelsInResponse:(CTResponse *)aResponse
{
    NSArray* result = nil;
    if([self.delegate respondsToSelector:@selector(moduleList:processFetchedModelsInResponse:)])
        result = [self.delegate moduleList:self processFetchedModelsInResponse:aResponse];
    else
        result = aResponse.boArray;
    return result;
}

- (void)prepareSectionsForModels:(NSArray*)aModels
{
    [self.tableDisposer removeAllSections];
    if([self.delegate respondsToSelector:@selector(moduleList:prepareSectionsForModels:)])
    {
        NSArray* sections = [self.delegate moduleList:self prepareSectionsForModels:aModels];
        
        if([aModels.firstObject isKindOfClass:[NSArray class]])
        {
            NSAssert(sections.count == aModels.count, @"Sections count should be equal to subarrays count");
        }
        else
        {
            NSAssert(sections.count == 1, @"Need only one section for plain array of models");
        }
        
        for(CTSectionReadonly* section in sections)
        {
            [self.tableDisposer addSection:section];
        }
    }
    else
    {
        if([aModels.firstObject isKindOfClass:[NSArray class]])
        {
            for(NSUInteger i = 0; i < aModels.count; ++i)
            {
                [self.tableDisposer addSection:[CTSectionReadonly section]];
            }
        }
        else
        {
            [self.tableDisposer addSection:[CTSectionReadonly section]];
        }
    }
}

- (void)setupModels:(NSArray*)aModels forSection:(CTSectionReadonly *)aSection
{
//    models = [NSMutableArray arrayWithArray:aModels];
    if(self.tableDisposer.useCompoundCells)
        compoundModels = [NSMutableArray arrayWithArray:[CTCompoundModel compoundModelsFromModels:aModels
                                                                                   groupedByCount:self.defaultCompoundMaxModelsCount]];

    [self.tableDisposer setupModels:(self.tableDisposer.useCompoundCells) ? (compoundModels) : (aModels)
                         forSection:aSection];
//    [self.tableDisposer reloadData];
}

- (void)saveModels:(NSArray*)aModels
{
    models = [NSMutableArray arrayWithArray:aModels];
}


#pragma mark - CTTableDisposerModeledMulticastDelegate

- (void)tableDisposer:(CTTableDisposerModeled*)aTableDisposer didCreateCellData:(CTCellData*)aCellData
{
    if(aTableDisposer != self.tableDisposer)
        return;    
}

#pragma mark - CTSearchListStrategyDelegate

- (CTTableDisposerModeled *)moduleListTableDisposerForSearchStrategy:(CTSearchListStrategy *)searchStrategy
{
    return nil; // it can't be original tableDisposer - it will cause wrong reloading behavior
}

- (void)configureLoadedTableView:(UITableView *)aTableView forSearchStrategy:(CTSearchListStrategy *)searchStrategy
{
    if ([self.delegate respondsToSelector:@selector(configureLoadedTableView:forSearchStrategy:)])
        [self.delegate configureLoadedTableView:aTableView forSearchStrategy:searchStrategy];
    else
    {
        // default approach
        aTableView.autoresizingMask = defaultTableDisposer.tableView.autoresizingMask;
        aTableView.backgroundColor = defaultTableDisposer.tableView.backgroundColor;
        aTableView.separatorColor = defaultTableDisposer.tableView.separatorColor;
        aTableView.separatorStyle = defaultTableDisposer.tableView.separatorStyle;
    }
}

#pragma mark - CTModuleListSearchDelegate

- (void)searchListStrategyWillBeginSearch:(CTSearchListStrategy *)searchStrategy
{
    if (delegateSearch && [delegateSearch respondsToSelector:@selector(searchListStrategyWillBeginSearch:)])
    {
        [delegateSearch searchListStrategyWillBeginSearch:self];
    }
}
- (void)searchListStrategyDidBeginSearch:(CTSearchListStrategy *)searchStrategy
{
    if (delegateSearch && [delegateSearch respondsToSelector:@selector(searchListStrategyDidBeginSearch:)])
    {
        [delegateSearch searchListStrategyDidBeginSearch:self];
    }
}

- (void)searchListStrategyWillEndSearch:(CTSearchListStrategy *)searchStrategy
{
    if (delegateSearch && [delegateSearch respondsToSelector:@selector(searchListStrategyWillEndSearch:)])
    {
        [delegateSearch searchListStrategyWillEndSearch:self];
    }
    defaultTableDisposer.tableView.userInteractionEnabled = NO;
}

- (void)searchListStrategyDidEndSearch:(CTSearchListStrategy *)searchStrategy
{
    if (delegateSearch && [delegateSearch respondsToSelector:@selector(searchListStrategyDidEndSearch:)])
    {
        [delegateSearch searchListStrategyDidEndSearch:self];
    }
    [self.dataFetcher cancelFetching];
    defaultTableDisposer.tableView.userInteractionEnabled = YES;
}

- (void)searchBarSearchButtonClicked:(CTSearchBar *)aSearchBar
{
    if ([self.delegateSearch respondsToSelector:@selector(searchBarSearchButtonClicked:)])
    {
        [self.delegateSearch searchBarSearchButtonClicked:self];
    }
}

- (void)searchBarCancelButtonClicked:(CTSearchBar *)aSearchBar
{
    if ([self.delegateSearch respondsToSelector:@selector(searchBarCancelButtonClicked:)])
    {
        [self.delegateSearch searchBarCancelButtonClicked:self];
    }
}


- (BOOL)searchListStrategy:(CTSearchListStrategy *)searchStrategy shouldReloadTableForSearchString:(NSString *)searchString andSearchScope:(NSInteger)searchOption
{
    [self.tableDisposer removeAllSections];
    [self.tableDisposer reloadData];

    if ([searchString length] >= minSearchStringLength)
    {
        [self reloadData];
    }

    return NO;
}



@end
