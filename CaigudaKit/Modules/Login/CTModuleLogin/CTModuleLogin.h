//
//  CTModuleLogin.h
//  CaigudaKit
//
//  Created by Malaar on 11.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTTextField.h"
#import "CTValidationGroup.h"
#import "CTKeyboardAvoidingScrollView.h"
#import "CTGatewayRequest.h"
#import "CTActivityAdapter.h"
#import "CTHelper.h"

@class CTModuleLogin;

typedef void (^CTModuleLoginInvalidFieldCallback) (CTTextField* anInvalidTextField);
typedef CTGatewayRequest* (^CTModuleLoginRequestConfigurator)(NSString* aLogin, NSString* aPassword, __weak CTModuleLogin *moduleLogin);

/**
 * GUI layer
 * Use this module to manage login process.
 **/
@interface CTModuleLogin : NSObject <UITextFieldDelegate>
{
    __weak CTTextField* loginField;
    __weak CTTextField* passwordField;
    __weak UIButton* loginButton;
    
    CTValidator* defaultLoginValidator;
    CTValidator* defaultPasswordValidator;
    
    CTModuleLoginRequestConfigurator requestConfigurator;
}

#pragma mark - Validation logic
@property (nonatomic, strong, readonly) CTValidationGroup* validationGroup;
@property (nonatomic, copy) CTModuleLoginInvalidFieldCallback invalidLoginCallback;
@property (nonatomic, copy) CTModuleLoginInvalidFieldCallback invalidPasswordCallback;

@property (nonatomic, strong) NSString* internetUnreachableAlert;

@property (nonatomic, strong) CTActivityAdapter* activityAdapter;

#pragma mark - Settings
@property (nonatomic, assign) BOOL shouldLoginOnGoPressed;
@property (nonatomic, assign) BOOL autoHideActivityAdapter; // by default is YES

#pragma mark - Fields
@property (nonatomic, readonly) CTTextField* loginField;
@property (nonatomic, readonly) CTTextField* passwordField;
@property (nonatomic, readonly) UIButton* loginButton;

/**
 * Use this method to initialize module with defaul login (CTValidatorEmail) and password (CTValidatorNotEmpty) validators.
 **/
- (id)initWithRequestConfigurator:(CTModuleLoginRequestConfigurator)aRequestConfigurator;

/**
 * Use this method to initialize module with custom login and password validators.
 **/
- (id)initWithLoginValidator:(CTValidator*)aLoginValidator
           passwordValidator:(CTValidator*)aPasswordValidator
         requestConfigurator:(CTModuleLoginRequestConfigurator)aLoginRequestConfigurator;

/**
 * Use this method to setup textfields for login and password.
 * Call this method in -viewDidLoad method of view controller
 * Received fields don't retain (used weak reference)
 **/
- (void)configureWithLoginField:(CTTextField*)aLoginField
                  passwordField:(CTTextField*)aPasswordField
                    loginButton:(UIButton*)aLoginButton
                 controllerView:(UIView*)aControllerView;

- (void)releaseViews;

/**
 * Use this method only to login programmatically
 **/
- (void)login;

@end
