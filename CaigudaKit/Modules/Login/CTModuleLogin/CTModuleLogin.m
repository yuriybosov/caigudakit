//
//  CTModuleLogin.m
//  CaigudaKit
//
//  Created by Malaar on 11.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleLogin.h"

@interface CTModuleLogin ()

- (void)loginButtonPressed:(UIButton*)aSender;

@end

@implementation CTModuleLogin

@synthesize validationGroup;
@synthesize invalidLoginCallback, invalidPasswordCallback;
@synthesize internetUnreachableAlert;
@synthesize shouldLoginOnGoPressed, activityAdapter;

@synthesize loginField, passwordField, loginButton;

#pragma mark - Init/Dealloc

- (id)initWithRequestConfigurator:(CTModuleLoginRequestConfigurator)aRequestConfigurator
{
    self = [self initWithLoginValidator:[CTValidatorEmail new]
                      passwordValidator:[CTValidatorNotEmpty new]
                    requestConfigurator:aRequestConfigurator];
    return self;
}

- (id)initWithLoginValidator:(CTValidator*)aLoginValidator
           passwordValidator:(CTValidator*)aPasswordValidator
         requestConfigurator:(CTModuleLoginRequestConfigurator)aRequestConfigurator
{
    self = [super init];
    if(self)
    {
        defaultLoginValidator = aLoginValidator;
        defaultPasswordValidator = aPasswordValidator;
        requestConfigurator = aRequestConfigurator;
        
        self.autoHideActivityAdapter = YES;
        
        invalidLoginCallback = ^(CTTextField* anInvalidTextField)
        {
            if(anInvalidTextField.validator.errorMessage)
                CTShowSimpleAlert(nil, anInvalidTextField.validator.errorMessage);
            else
                CTShowSimpleAlert(nil, NSLocalizedString(@"Invalid login", nil));
        };
        invalidPasswordCallback = ^(CTTextField* anInvalidTextField)
        {
            if(anInvalidTextField.validator.errorMessage)
                CTShowSimpleAlert(nil, anInvalidTextField.validator.errorMessage);
            else
                CTShowSimpleAlert(nil, NSLocalizedString(@"Invalid password", nil));
        };
        internetUnreachableAlert = NSLocalizedString(@"No internet connection", nil);
    }
    return self;
}

#pragma mark - Configure

- (void)configureWithLoginField:(CTTextField*)aLoginField
                  passwordField:(CTTextField*)aPasswordField
                    loginButton:(UIButton*)aLoginButton
                 controllerView:(UIView*)aControllerView
{
    //
    [activityAdapter configureWithView:aControllerView];
    //
    
    loginField = aLoginField;
    passwordField = aPasswordField;
    loginButton = aLoginButton;
    
    //
    if(!loginField.validator)
        loginField.validator = defaultLoginValidator;
    if(!passwordField.validator)
        passwordField.validator = defaultPasswordValidator;
    
    NSArray* validators = @[loginField.validator, passwordField.validator];
    validationGroup = [[CTValidationGroup alloc] initWithValidators:validators];
    
    [loginButton addTarget:self action:@selector(loginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if([aControllerView isKindOfClass:[CTKeyboardAvoidingScrollView class]])
    {
        CTKeyboardAvoidingScrollView* scrollView = (CTKeyboardAvoidingScrollView*)aControllerView;
        [scrollView addObjectForKeyboard:loginField];
        [scrollView addObjectForKeyboard:passwordField];
    }
    
    if(shouldLoginOnGoPressed)
    {
        passwordField.ctdelegate = self;
        passwordField.returnKeyType = UIReturnKeyGo;
    }
}

- (void)releaseViews
{
    validationGroup = nil;
    [activityAdapter releaseActivityView];
}

#pragma mark - Login actions

- (void)login
{
    NSArray* errorFields = [validationGroup validate];
    
    if (errorFields.count)
    {
        CTTextField* textField = [errorFields objectAtIndex:0];
        CTModuleLoginInvalidFieldCallback invalidCallback = (textField == loginField) ? (invalidLoginCallback) : (invalidPasswordCallback);
        if (invalidCallback)
            invalidCallback(textField);
    }
    else
    {
        [loginField resignFirstResponder];
        [passwordField resignFirstResponder];
        
        __weak CTModuleLogin *weakself = self;
        CTGatewayRequest* loginRequest = requestConfigurator(loginField.text, passwordField.text, weakself);
        
        if([loginRequest isInternetReachable])
        {
            if (self.autoHideActivityAdapter)
            {
                [loginRequest addResponseBlock:^(CTResponse *aResponse)
                 {
                     CTModuleLogin *strongself = weakself;
                     if (strongself)
                         [strongself->activityAdapter hideActivity:YES];
                 
                 } responseQueue:dispatch_get_main_queue()];
            }
            
            [activityAdapter showActivity:YES];
            [loginRequest start];
        }
        else
        {
            CTShowSimpleAlert(nil, internetUnreachableAlert);
        }
        
    }
}

- (void)loginButtonPressed:(UIButton*)aSender
{
    [self login];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == passwordField)
    {
        [self login];
    }
    return YES;
}

- (void)setShouldLoginOnGoPressed:(BOOL)aShouldLoginOnGoPressed
{
    shouldLoginOnGoPressed = aShouldLoginOnGoPressed;
    if(shouldLoginOnGoPressed)
    {
        passwordField.ctdelegate = self;
        passwordField.returnKeyType = UIReturnKeyGo;
    }else
    {
        passwordField.ctdelegate = nil;
        passwordField.returnKeyType = UIReturnKeyDone;
    }
}

@end
