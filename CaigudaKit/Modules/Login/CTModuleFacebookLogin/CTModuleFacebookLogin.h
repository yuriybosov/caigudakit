//
//  CTModuleFacebookLogin.h
//  CaigudaKit
//
//  Created by Malaar on 30.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Module to login via Facebook
 **/
@interface CTModuleFacebookLogin : NSObject

@end
