//
//  CTFetcherWithBlock.h
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTDataFetcher.h"

typedef void (^CTDataFetchBlock)(CTFetcherMessage *aMessage, CTDataFetchCallback aCallback);

/**
 * Use this to determine fetching logic in block
 **/
@interface CTFetcherWithBlock : NSObject <CTDataFetcher>

@property (nonatomic, copy) CTDataFetchBlock fetchBlock;

- (id)initWithFetchBlock:(CTDataFetchBlock)aFetchBlock;

@end
