//
//  CTFetcherMessagePaging.h
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherMessage.h"

@interface CTFetcherMessagePaging : CTFetcherMessage

@property (nonatomic, assign) NSUInteger pagingSize;
@property (nonatomic, assign) NSUInteger pagingOffset;
@property (nonatomic, assign) BOOL reloading;
@property (nonatomic, assign) BOOL loadingMore;

@end