//
//  CTFetcherWithBlock.m
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherWithBlock.h"

@implementation CTFetcherWithBlock

@synthesize callbackQueue;

- (id)initWithFetchBlock:(CTDataFetchBlock)aFetchBlock
{
    self = [super init];
    if(self)
    {
        self.fetchBlock = aFetchBlock;
    }
    return self;
}

- (void)dealloc
{
    self.callbackQueue = NULL;
}

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
- (void)setCallbackQueue:(dispatch_queue_t)aCallbackQueue
{
    if (callbackQueue != aCallbackQueue)
    {
        if (callbackQueue)
        {
            dispatch_release(callbackQueue);
            callbackQueue = NULL;
        }
        
        if (aCallbackQueue)
        {
            dispatch_retain(aCallbackQueue);
            callbackQueue = aCallbackQueue;
        }
    }
}
#endif

- (BOOL)canFetchWithMessage:(CTFetcherMessage *)aMessage
{
    return self.fetchBlock != nil;
}

- (void)fetchDataByMessage:(CTFetcherMessage*)aMessage
              withCallback:(CTDataFetchCallback)aFetchCallback
{
    NSAssert(self.callbackQueue, @"CTFetcherWithBlock: callbackQueue is nil!");
    
    dispatch_async(self.callbackQueue, ^
    {
        self.fetchBlock(aMessage, aFetchCallback);
    });
}

- (void)cancelFetching
{
}

@end
