//
//  CTDataFetchStrategy.h
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTDataFetcher.h"
#import "CTRequest.h"
#import "CTFetcherMessage.h"

/**
 * This is strategy that determine fetch data with request.
 * Request can be CTGatewayRequest or CTDataBaseRequest.
 * WARNING: Setup callbackQueue before setup request.
 **/
@interface CTFetcherWithRequest : NSObject <CTDataFetcher>
{
    CTRequest* preparedRequest;
    CTRequest* request;
    
    CTDataFetchCallback fetchCallback;
}

@property (nonatomic, strong) CTRequest* request;
@property (nonatomic, readonly) CTDataFetchCallback fetchCallback;

- (CTRequest*)preparedRequestByMessage:(CTFetcherMessage*)aMessage;

@end
