//
//  CTQueueNode.m
//  CaigudaKit
//
//  Created by Alexander Burkhai on 8/6/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTQueueNode.h"

@implementation CTQueueNode

@synthesize queueTag;

- (void)dealloc
{
    self.dispatchQueue = NULL;
}

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
- (void)setDispatchQueue:(dispatch_queue_t)dispatchQueue
{
    if (_dispatchQueue != dispatchQueue)
    {
        if (_dispatchQueue)
        {
            dispatch_release(_dispatchQueue);
            _dispatchQueue = NULL;
        }
        
        if (dispatchQueue)
        {
            dispatch_retain(dispatchQueue);
            _dispatchQueue = dispatchQueue;
            
            queueTag = &queueTag;
            dispatch_queue_set_specific(_dispatchQueue, queueTag, queueTag, NULL);
        }
    }
}
#else
- (void)setDispatchQueue:(dispatch_queue_t)dispatchQueue
{
    _dispatchQueue = dispatchQueue;
    if (_dispatchQueue)
    {
        queueTag = &queueTag;
        dispatch_queue_set_specific(_dispatchQueue, queueTag, queueTag, NULL);
    }
}
#endif

@end
