//
//  CTCompoundValidator.h
//  CaigudaKit
//
//  Created by Alexander Burkhai on 6/12/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTValidator.h"

@interface CTCompoundValidator : CTValidator
{
	NSArray* validators;
}

@property (nonatomic, assign) BOOL successIfAtLeastOne; // by default is YES

- (id)initWithValidators:(NSArray*)aValidators;

@end
