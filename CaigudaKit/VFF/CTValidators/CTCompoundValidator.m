//
//  CTCompoundValidator.m
//  CaigudaKit
//
//  Created by Alexander Burkhai on 6/12/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTCompoundValidator.h"

@implementation CTCompoundValidator

- (id)initWithValidators:(NSArray *)aValidators
{
    self = [super init];
    if (self)
    {        
        validators = [NSArray arrayWithArray:aValidators];
        self.successIfAtLeastOne = YES;
    }
    return self;
}

- (BOOL)validate
{
    NSAssert([validators count], @"count of validators should be more than 0");
    
    BOOL result = (self.successIfAtLeastOne) ? NO : YES;
    for (CTValidator *validator in validators)
    {
        validator.validatableObject = self.validatableObject;
        BOOL valid = [validator validate];
        if (valid && self.successIfAtLeastOne)
            return YES;
        else if (!valid && !self.successIfAtLeastOne)
            return NO;
    }
    
    return result;
}

@end
