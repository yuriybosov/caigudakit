//
//  CTValidationGroup.h
//  CaigudaKit
//
//  Created by Malaar on 10.07.11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTValidator.h"

@protocol CTValidationGroupDelegate;

@interface CTValidationGroup : NSObject
{
    NSMutableArray* validators;
    UIImage* invalidIndicatorImage;
}

@property (nonatomic, retain) UIImage* invalidIndicatorImage;
@property (nonatomic, assign) NSObject<CTValidationGroupDelegate>* delegate;

- (id)initWithValidators:(NSArray*)aValidators;

- (void)addValidator:(CTValidator*)aValidator;
- (void)addValidators:(NSArray*)aValidators;
- (void)removeValidator:(CTValidator*)aValidator;
- (void)removeAllValidators;

- (NSArray*)validate;
- (void)hideInvalidIndicators;

- (void)showInvalidViewForField:(UITextField*)aTextField;
- (void)hideInvalidViewForField:(UITextField*)aTextField;

@end


@protocol CTValidationGroupDelegate <NSObject>
@optional
- (void)proccessValidationResults:(NSMutableArray*)aValidationResults;
- (void)prepareInvalidIndicatorView:(UITextField *)aTextField;
@end

