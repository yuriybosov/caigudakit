//
//  MUInputTextFilter.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 4/18/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * text filter : base class
 **/
@interface CTFilter : NSObject
{
    NSUInteger maxLengthText;
}

@property (nonatomic, assign) NSUInteger maxLengthText;

- (id)initWithMaxLengthText:(NSUInteger)aMaxLengthText;
- (BOOL)textInField:(id)inputField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string;

@end

/**
 * text with only number
 **/
@interface CTFilterNumbers : CTFilter

@end

/**
 * text with only letters
 **/
@interface CTFilterLetters : CTFilter

@end

/**
 *
 **/
@interface CTFilterLettersAndDigits : CTFilter

@end

/**
 *
 **/
@interface CTFilterRegExp : CTFilter
{
    NSRegularExpression *regExp;
}

@property (nonatomic, readonly) NSRegularExpression *regExp;

- (id)initWithMaxLengthText:(NSUInteger)aMaxLengthText
                  andRegExp:(NSRegularExpression *)aRegExp;

@end

/**
 *
 **/
@interface CTFilterWithEditableRange : CTFilter
{
    NSRange editableRange;
    NSCharacterSet *acceptableCharacters;
}

@property (nonatomic, assign) NSRange editableRange;

- (id)initWithEditableRange:(NSRange)anEditableRange;

@end

/**
 *
 **/
@interface CTFilterNumbersWithEditableRange : CTFilterWithEditableRange

@end
