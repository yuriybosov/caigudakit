//
//  MUCreditCardNumberFormatter.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 2/5/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//



@interface CTCreditCardNumberFormatter : NSObject

- (BOOL)cardFormatForTextField:(UITextField *)textField
 shouldChangeCharactersInRange:(NSRange)range
             replacementString:(NSString *)string
               separatorString:(NSString *)separatorString;
@end
