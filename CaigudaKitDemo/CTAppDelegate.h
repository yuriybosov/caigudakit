//
//  CTAppDelegate.h
//  CaigudaKitDemo
//
//  Created by Malaar on 11.02.14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
